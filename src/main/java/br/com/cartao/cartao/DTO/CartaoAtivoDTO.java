package br.com.cartao.cartao.DTO;

public class CartaoAtivoDTO {
    private Boolean ativo;

    public CartaoAtivoDTO() {
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
