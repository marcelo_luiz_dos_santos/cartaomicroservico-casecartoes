package br.com.cartao.cartao.service;

import br.com.cartao.cartao.Client.Cliente;
import br.com.cartao.cartao.Client.ClienteClient;
import br.com.cartao.cartao.DTO.CartaoAtivoDTO;
import br.com.cartao.cartao.DTO.CartaoGetDTO;
import br.com.cartao.cartao.model.Cartao;
import br.com.cartao.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao criarCartao(Cartao cartao){
            Cliente cliente = clienteClient.buscaPorId(cartao.getClienteId());
            cartao.setAtivo(false);
            Cartao cartaoObjeto = cartaoRepository.save(cartao);
            return cartaoObjeto;
    }

    public CartaoGetDTO buscarPorId(int id) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);
        if (optionalCartao.isPresent()){
            CartaoGetDTO cartaoGetDTO = new CartaoGetDTO();

            cartaoGetDTO.setId(optionalCartao.get().getId());
            cartaoGetDTO.setNumero(optionalCartao.get().getNumero());
            cartaoGetDTO.setClienteId(optionalCartao.get().getClienteId());

            return cartaoGetDTO;
        }
        throw new RuntimeException("O cartão não foi encontrado");
    }

    public Cartao ativarOuDesativarCartao(int id, CartaoAtivoDTO cartaoDTO){
        if (cartaoRepository.existsById(id)){
            Optional<Cartao> cartaoAtual = cartaoRepository.findById(id);

            Cartao cartao = new Cartao();
            cartao.setId(cartaoAtual.get().getId());
            cartao.setClienteId(cartaoAtual.get().getClienteId());
            cartao.setNumero(cartaoAtual.get().getNumero());
            cartao.setAtivo(cartaoDTO.getAtivo());

            Cartao cartaoObjeto = cartaoRepository.save(cartao);
            return cartaoObjeto;
        }
        throw new RuntimeException("O cartão não foi encontrado");
    }


    public CartaoGetDTO buscarPorIdAndAtivo(int id, Boolean ativo) {
        Optional<Cartao> optionalCartao = cartaoRepository.findByIdAndAtivo(id, ativo);
        if (optionalCartao.isPresent()){
            CartaoGetDTO cartaoGetDTO = new CartaoGetDTO();

            cartaoGetDTO.setId(optionalCartao.get().getId());
            cartaoGetDTO.setNumero(optionalCartao.get().getNumero());
            cartaoGetDTO.setClienteId(optionalCartao.get().getClienteId());

            return cartaoGetDTO;
        }
        throw new RuntimeException("O cartão não foi encontrado ou não está ativo");
    }
}

