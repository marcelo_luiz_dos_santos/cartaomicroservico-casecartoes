package br.com.cartao.cartao.Client;

import com.netflix.client.ClientException;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClienteClientLoadBalancerFallback implements ClienteClient {
    private Exception exception;

    public ClienteClientLoadBalancerFallback(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Cliente buscaPorId(int id) {
        if(exception.getCause() instanceof ClientException) {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço CLIENTE indisponível temporariamente");
        }
        throw (RuntimeException) exception;
    }

}
