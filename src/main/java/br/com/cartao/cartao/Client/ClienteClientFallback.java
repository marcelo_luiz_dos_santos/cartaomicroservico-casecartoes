package br.com.cartao.cartao.Client;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClienteClientFallback implements ClienteClient{

    @Override
    public Cliente buscaPorId(int id) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço CLIENTE indisponível");
    }

}
