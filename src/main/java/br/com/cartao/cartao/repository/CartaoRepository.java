package br.com.cartao.cartao.repository;

import br.com.cartao.cartao.model.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Optional<Cartao> findByIdAndAtivo(int id, Boolean ativo);
}
