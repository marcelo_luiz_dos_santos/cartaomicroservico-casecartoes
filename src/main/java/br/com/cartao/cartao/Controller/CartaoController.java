package br.com.cartao.cartao.Controller;

import br.com.cartao.cartao.Client.ClienteClient;
import br.com.cartao.cartao.DTO.CartaoAtivoDTO;
import br.com.cartao.cartao.DTO.CartaoGetDTO;
import br.com.cartao.cartao.model.Cartao;
import br.com.cartao.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao criarCartao(@RequestBody @Valid Cartao cartao){
            Cartao cartaoObjeto = cartaoService.criarCartao(cartao);
            return cartaoObjeto;
    }

    @GetMapping("/{id}")
    public CartaoGetDTO bucarPorId(@PathVariable(name = "id") int id){
        try{
            CartaoGetDTO cartao = cartaoService.buscarPorId(id);
            return cartao;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}/{ativo}")
    public CartaoGetDTO buscarPorIdAndAtivo(@PathVariable(name = "id") int id, @PathVariable(name = "ativo") Boolean ativo){
        try{
            CartaoGetDTO cartao = cartaoService.buscarPorIdAndAtivo(id, ativo);
            return cartao;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public Cartao ativaOuDesativaCartao(@PathVariable(name = "id") int id, @RequestBody CartaoAtivoDTO cartaoDTO){
        try{
            Cartao cartaoObjeto = cartaoService.ativarOuDesativarCartao(id, cartaoDTO);
            return cartaoObjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
